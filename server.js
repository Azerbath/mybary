if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config({path: __dirname + '/.env'})
}
const express= require('express');
const app= express();
const expressLayouts= require('express-ejs-layouts');
const indexRouter=require('./routes/index');
app.set('view engine','ejs');
app.set('views',__dirname + '/views');
app.set('layout','layouts/layout');
app.use(expressLayouts);
app.use(express.static('public'));
console.log(process.env.MONGO_ATLAB_PW);
console.log(process.env.NODE_ENV);

const mongoose=require('mongoose');
mongoose.connect('mongodb://nodejs:mongodbs@cluster0-shard-00-00-tpgzk.mongodb.net:27017,cluster0-shard-00-01-tpgzk.mongodb.net:27017,cluster0-shard-00-02-tpgzk.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority');
const db=mongoose.connection;
db.on('error',error=> console.error(error));
db.once('open',()=>console.log('Connected to Mongoose'));

app.use('/',indexRouter);
app.listen(process.env.PORT || 3000);
